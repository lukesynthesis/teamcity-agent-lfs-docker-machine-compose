# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the source for the docker image that extends the official teamcity-agent image and adds git-lfs support. It also adds docker-machine and docker-compose support. This allows you to streamline the use of git for release management of large artefacts like WAR files etc.

### How do I get set up? ###

* Clone the repo
* Build the docker file.
* Push the docker file.
* On Windows, there are 3 batch files that you can run in sequence.

### Who do I talk to? ###

* Luke Machowski <luke@synthesis.co.za>
* www.synthesis.co.za