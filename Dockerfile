# This creates a teamcity agent that also has git-lfs, docker-machine and docker-compose support.
FROM synthesis/teamcity-agent-lfs
MAINTAINER Luke Machowski <luke@synthesis.co.za>

# Install git-lfs according to these instructions:
# https://docs.docker.com/machine/install-machine/
# https://docs.docker.com/compose/install/


# Download docker-compose and docker-machine:
RUN curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose && \
	chmod +x /usr/local/bin/docker-compose && \
	curl -L https://github.com/docker/machine/releases/download/v0.7.0/docker-machine-`uname -s`-`uname -m` > /usr/local/bin/docker-machine && \
	chmod +x /usr/local/bin/docker-machine