REM Set up the docker shell environment:
REM docker-machine env --shell cmd default
FOR /f "tokens=*" %%i IN ('docker-machine env --shell cmd default') DO %%i

REM Push the image to the registry:
call docker push synthesis/teamcity-agent-lfs-docker-machine-compose