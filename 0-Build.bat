@echo off

REM Set up the docker shell environment:
REM docker-machine env --shell cmd default
FOR /f "tokens=*" %%i IN ('docker-machine env --shell cmd default') DO %%i

REM Build the docker image:
call docker build --tag=synthesis/teamcity-agent-lfs-docker-machine-compose .