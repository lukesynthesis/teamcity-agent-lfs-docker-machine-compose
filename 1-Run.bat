@echo off

REM Set up the docker shell environment:
REM docker-machine env --shell cmd default
FOR /f "tokens=*" %%i IN ('docker-machine env --shell cmd default') DO %%i

REM Run the teamcity agent
call docker run -it -e SERVER_URL="http://teamcity-server:8111" -e AGENT_NAME="build-agent" -p 12348:9090 synthesis/teamcity-agent-lfs-docker-machine-compose